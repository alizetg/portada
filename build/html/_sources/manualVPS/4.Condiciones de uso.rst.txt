*********************
8. Condiciones de uso
*********************

Todas las personas que hagan uso de los recursos proporcionados por el CeSViMa asumen las siguientes responsabilidades:

1.	Todos los usuarios han leído, entendido y aceptan las políticas de uso aceptable y todos sus anexos.

2.	El trabajo realizado en los recursos proporcionados por el CeSViMa (incluyendo todos los equipos personales, estaciones de trabajo, servidores y dispositivos proporcionados y/o gestionados por el CeSViMa) debe disponer de una autorización previa.

3.	Cuando el trabajo realizado utilizando los recursos proporcionados por el CeSViMa derive en publicaciones, patentes, programas o similares es imprescindible incluir un agradecimiento utilizando, única y exclusivamente, una de las siguientes fórmulas:


	* **Proyectos CeSViMa:**

	    * Castellano:
	    	
           **El autor con agradecimiento reconoce los recursos informáticos, conocimientos técnicos y asistencia proporcionada por el Centro de Supercomputación y Visualización de Madrid (CeSViMa).**

	    * Inglés:
	    	
           **The author thankfully acknowledges the computer resources, technical expertise and assistance provided by the Supercomputing and Visualization Center of Madrid (CeSViMa).**


	* **Proyectos RES:**
		
		* Castellano:
	  		 
           **El autor con agradecimiento reconoce los recursos informáticos, conocimientos técnicos y asistencia proporcionada por el Centro de Supercomputación y Visualización de Madrid (CeSViMa) y la Red Española de Supercomputación (RES).**

	    * Inglés:
		   	
           **The author thankfully acknowledges the computer resources, technical expertise and assistance provided by the Supercomputing and Visualization Center of Madrid (CeSViMa) and the Spanish Supercomputing Network (RES).**

    
    La información referente a dichos artículos debe enviarse a la dirección de soporte para su inclusión en las memorias anuales del centro e informes de control internos.


4.	Además de las publicaciones, el usuario debe proporcionar cualquier tipo de material publicado cuando lo solicite el personal del CeSViMa.



5. 	El usuario es responsable de la seguridad de sus programas y datos, y debe tomar todas las precauciones necesarias para protegerlos. En particular, el *password* y otras credenciales utilizadas para acceder a cuentas del centro debe ser protegido y **nunca**, bajo ninguna circunstancia, se debe compartir.
	Ante cualquier sospecha de un compromiso de seguridad en su sistema, *passwords*, credenciales o datos, el usuario se compromete a comunicar inmediatamente cualquier uso no autorizado, pérdida, robo o extravío de la autenticación. El usuario será responsable de cualquier daño producido al CeSViMa o cualquier daño resultante del incumplimiento de estas políticas.



6.	Está prohibida la compilación e instalación de software en el sistema sin autorización previa del CeSViMa. En ningún caso se autorizará la posesión de un software protegido en el sistema si no se presenta una copia de la correspondiente licencia.



7.	Se podrá recopilar información de rendimiento de los programas y trabajos en todas las ejecuciones.


8.1 Condiciones especiales para usuarios CeSViMa
################################################

Cuando el usuario accede al sistema a través del propio CeSViMa, en la propia solicitud de apertura de cuenta, firma su aceptación de las siguientes:

1.	El uso de los sistemas implica respetar las condiciones de las licencias.

2.	Las cuentas de usuario son personales e intransferibles. No se debe proporcionar la clave de acceso a terceras partes.

3.	Si existiese sospecha de que personas no autorizadas han utilizado o intentado utilizar los recursos, debe ser notificado inmediatamente a CeSViMa.

4.	Los recursos asignados deben utilizarse únicamente para las tareas propuestas.

5.	Debido a condicionantes de licencias, los usuarios extranjeros y el uso de los recursos desde el extranjero deben negociarse de manera separada.

6.	La cuenta de usuario debe protegerse con una clave de acceso razonablemente segura.

7.	Cierto software sólo puede utilizarse en entornos académicos. Para el uso por parte de otros usuarios deberá consultarse con CeSViMa.

8.	CeSViMa no realiza copias de salvaguarda de los ficheros de usuario. En cualquier caso, CeSViMa declina toda responsabilidad por la pérdida de ficheros debido a fallos del sistema.

Además de las normas generales del Centro y de las aplicables a usuarios del CeSViMa, los líderes de proyecto firman su conformidad con las normas:

1.	Asegurar que los miembros del proyecto siguen las normas de usuarios y seguridad establecidas.

2.	Verificar los informes de usuario y supervisar que la utilización es la adecuada.

3.	Cualquier cambio en la información de contacto debe ser comunicada inmediatamente por correo electrónico al Centro de atención a usuarios [#f1]_ .
4.	El responsable del proyecto informará periódicamente del progreso del trabajo mediante el envío de un breve informe de actividad y resultados obtenidos cada cuatro meses [#f2]_ .

Los proyectos que no remitan dicho informe, antes del vencimiento del periodo en vigor, se entenderá que han finalizado o no tienen interés en seguir utilizando los recursos por lo que, tras un plazo de quince días, se procederá a bloquear y borrar las cuentas asignadas al mismo.

El informe deberá incluir la referencia de los proyectos bajo los cuales se han realizado las actividades, así como los resultados obtenidos, tales como publicaciones, patentes, programas, etc. y una copia del trabajo publicado.

En función de los resultados proporcionados en los informes se podrán asignar prioridades en el gestor de colas y, por tanto, del uso del sistema.

.. [#f1] support@cesvima.upm.es

.. [#f2] Las fechas límites son el primer día de los meses de marzo, julio y noviembre.


8.2 Condiciones especiales para usuarios RES
############################################


Si el usuario accede al sistema en virtud del acuerdo con la RES, son de aplicación las condiciones:

1.	Deberá aceptar y enviar firmado el documento *User responsibilities* (disponible en el área `RES <http://www.res.es/>`_) que implica un compromiso expreso de aceptación de las políticas impuestas por la RES. Si no se envía dicho documento en el plazo de 15 días se suprimirá la cuenta afectada.

2.	El responsable del proyecto debe informar periódicamente del progreso del trabajo realizado. Para ello es necesario enviar un informe en el área RES al menos cada 15 días. Si no se reciben los informes se deshabilitará el acceso a las colas del sistema.


