****************************
2. Visión general de los VPS
****************************

El supercomputador Magerit es un clúster de propósito general con arquitectura dual (Intel y POWER) que permite cubrir la mayor parte de las necesidades de cómputo.

Además de la capacidad de almacenamiento local de cada nodo, el sistema dispone de servidores de disco que dan acceso a un total de 432 TB de disco compartido entre todos los nodos.

La configuración POWER es capaz de proporcionar una potencia sostenida de más de 72 TFLOPS sobre un pico teórico de casi 103‘5 TFLOPS [#f3]_ . La partición Intel proporciona una potencia sostenida de más de 14’8 TFLOPS sobre un pico teórico de 15’9 TFLOPS.

.. [#f3]  http://top500.org/system/177311

3.1 Nodos
#########

Magerit tiene dos tipos diferentes de nodos con distintas características cada uno:

* **Arquitectura POWER7**

	245 nodos BladeCenter PS702 cada uno de los cuales dispone de 2 procesadores con 8 cores cada uno (16 cores) POWER7 de 3‘3 GHz (422’4 GFlops) con 32 GB de RAM y 300 GB de disco duro local.

* **Arquitectura Intel**

	41  nodos con 2 procesadores Intel Xeon E5-2670 de 8 cores a 2.6GHz (332 GFlops) y con 64 GB de RAM.

Aunque todos los nodos de una misma arquitectura tienen una configuración hardware y software idéntica, se dividen en dos funcionalidades básicas:

* **Interactivos o de login**

	Tienen habilitado el acceso al exterior y se utilizan como punto de entrada al sistema. En ellos se realizan labores de edición, compilación, gestión de trabajos e intercambio de ficheros.
	
	En estos nodos no se permite la ejecución de servicios o cálculos, que son cancelados de forma automática.
	Existen nodos interactivos para cada una de las dos arquitecturas de la máquina:
	
	* Intel:	xmagerit.cesvima.upm.es
	
	* POWER:	pmagerit.cesvima.upm.es
	
	Tras cada una de las dos direcciones se encuentran varias máquinas entre las que se repartirá la carga a través de un mecanismo de asignación round-robin.
	
	La conexión del CeSViMa con el exterior se realiza a través de la Universidad Politécnica de Madrid (UPM) y RedIRIS, mediante enlaces de 1Gbps a los que sólo tienen acceso estos nodos interactivos.

* **Cómputo**

	Tienen como única misión ejecutar los trabajos de usuario. Estos nodos están completamente aislados del exterior y sólo son accesibles desde el gestor de trabajos.

3.2 Sistema de ficheros
#######################

Todos los nodos tienen acceso a un espacio de almacenamiento compartido y distribuido con una capacidad bruta total de 432 TB, que utiliza un sistema distribuido y tolerante a fallos denominado GPFS[#f4]_ .

Cada proyecto tiene acceso a dos ubicaciones diferenciadas:

* **/home**

	Contiene toda la información de cada uno de los proyectos. Cada proyecto dispone de su propia entrada en esta ubicación.
	
	Dentro de cada directorio de proyecto se encuentran los directorios personales (home) de los usuarios que pueden utilizarse para almacenar la configuración del sistema y sus datos personales.
	
	Dentro de cada proyecto existen dos directorios especiales:

		*	/home/<código_proyecto>/PROJECT

		Proporciona un espacio compartido por todos los miembros del proyecto, por lo que puede utilizarse para almacenar los datos o código que sean usados por múltiples usuarios del mismo proyecto.

		*	/home/<código_proyecto>/SCRATCH

		Es el espacio de almacenamiento temporal. Cada usuario puede utilizar este espacio para almacenar información necesaria durante la ejecución de los trabajos.

		Los archivos con una antigüedad superior a una semana pueden ser eliminados automáticamente.
	
	El sistema de ficheros GPFS se encuentra controlado por un sistema de cuotas asignadas a cada grupo, es decir, se considera el total de espacio usado independientemente del miembro que lo utiliza. La coordinación del uso de este espacio de almacenamiento recae sobre el investigador responsable del proyecto.


.. topic:: Nota:

    No se proporciona servicio de backup de ninguno de los sistemas de ficheros, por lo que es responsabilidad de cada usuario y/o investigador principal, realizar y gestionar sus propias copias de seguridad.


* **/sw**

	Las aplicaciones y bibliotecas que proporcione la distribución del sistema operativo se encontrarán en sus rutas originales.
	
	Por otro lado, el software adicional que se ha instalado en el sistema se encuentra disponible en el directorio /sw. Dentro del mismo, se encontrarán agrupadas las aplicaciones atendiendo a la biblioteca de paso de mensajes disponibles para ese sistema.
	
	Las aplicaciones que no presenten limitaciones legales o contractuales, como puede ser la necesidad de una licencia de uso, estarán disponibles para todos los usuarios del sistema, mientras que las aplicaciones con estas restricciones estarán en directorios privados con control de acceso. Si un usuario desea utilizar una aplicación con restricciones deberá acreditar que está en posesión de una licencia válida.
	
	Para solicitar la instalación de nuevas aplicaciones es necesario contactar con el Centro de Atención a Usuarios a través de support@cesvima.upm.es.
	
	Las modificaciones especiales sobre aplicaciones no estarán disponibles, como norma general, en la zona pública y deberán instalarse en la zona personal del usuario o en la zona compartida de proyectos previa autorización expresa del CeSViMa.


.. [#f4]  IBM. General Parallel File System. http://www.ibm.com/software/products/software

3.3 Redes
#########

Para la interconexión de todos los elementos se dispone de varios routers para redes Ethernet e Infiniband. Estos routers dan soporte a dos redes diferentes que están accesibles desde los nodos:

* **Red Infiniband:**
	
	Es una red de alto rendimiento y baja latencia, utilizada para las comunicaciones de las aplicaciones paralelas. Proporciona un ancho de banda de 40 Gbps con latencias inferiores a 1 microsegundo.

* **Red Ethernet:**
	
	Se trata de dos redes que se utilizan para cargar el sistema operativo y acceder al sistema de ficheros GPFS, respectivamente.
	Los nodos interactivos disponen de una tercera conexión de red Ethernet hacia Internet
