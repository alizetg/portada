**********************
10. Guías y tutoriales
**********************

10.1 Recomendaciones para la elección de contraseñas
####################################################

El primer eslabón en la cadena de tareas a realizar para conseguir acceso no autorizado a un sistema informático, por ser normalmente el más débil, es la obtención de la contraseña de cuentas de usuario con el fin de usurparle la identidad y de esta forma tener acceso a todos los recursos que éste tuviera. Muchas de estas contraseñas son obtenidas fácilmente al tratarse de nombres comunes o datos personales del usuario fácilmente deducibles. Además, las claves elegidas se cambian rara vez o incluso nunca. Por ello, es cuestión de tiempo y pruebas que se adivine la contraseña y se adquiera acceso al sistema.

Este problema cobra importancia cuando se analizan con detenimiento las condiciones de uso del sistema. Al aceptarlas, el usuario se hace responsable del uso fraudulento y de los daños que se hagan con su cuenta.

Existen numerosas técnicas para atacar una contraseña. Desde ataques con diccionarios de claves comunes, heurísticas basadas en datos personales (nombre) o phishing. Sin embargo, siguiendo una serie de recomendaciones básicas, la mayoría de las técnicas pierden gran parte de su eficacia.

10.1.1 Características de un buen *password*
""""""""""""""""""""""""""""""""""""""""""""

Una buena *password* se caracteriza por:

* Es privado, esto es, conocido y usado por una única persona.

* Secreto, ya que no debe aparecer de forma no cifrada en ningún fichero, programa o papel.

* Fácilmente recordable.
 
* No adivinable o deducible por ningún programa en un tiempo razonable (más de una semana).

10.1.2 Recomendaciones
""""""""""""""""""""""

* *No utilizar contraseñas que sean palabras* en ningún idioma.

Tampoco es aconsejable utilizar el nombre del usuario o su seudónimo, nombres de familiares o personajes de ficción, mascotas, ciudades, el nombre del propio servicio, etc.

* *No utilizar contraseñas completamente numéricas* con algún   significado como el teléfono, DNI, fechas significativas (nacimiento, aniversarios), matrículas, etc.

Tampoco es aconsejable utilizar *passwords* completamente alfabéticos.

* *Deben ser contraseñas largas* de al menos 8 caracteres.

	Las contraseñas pequeñas pueden adivinarse en un lapso pequeño de tiempo.

* *Usar siempre una mezcla de caracteres numéricos y alfabéticos*

	Es aconsejable utilizar letras mayúsculas y minúsculas e incluso símbolos (puntos, guiones, arroba...) con lo que se incrementa el tiempo necesario para adivinarla.

		*  Todo mayúsculas o todo minúsculas

		*  Sólo el primer o el último carácter en mayúsculas

		*  Sólo las vocales en mayúsculas
	
		*  Sólo las consonantes en mayúsculas

*  *No utilizar la misma contraseña para más de un servicio*  siendo  aconsejable definir un conjunto de contraseñas básicas con variaciones lógicas según el servicio.

    De esta forma se evita el acceso a múltiples servicios del mismo usuario.

*  *Deben ser fáciles de recordar*  para evitar tener que escribirlas.


10.1.3 Proteger la contraseña
""""""""""""""""""""""""""""""

Además de elegir la contraseña adecuada (no adivinable por métodos automáticos), es necesario protegerla para evitar comprometer su seguridad con ingeniería social.

* *Nunca debe proporcionarse la contraseña* bajo ningún concepto ni requerimiento.

Esta es la base de las técnicas de phishing: solicitar de forma aparentemente oficial el ingreso en el sistema.

* *Nunca compartir con nadie la contraseña.*

Si se cree que alguien puede conocer la contraseña se debe cambiar inmediatamente.

* *No mantener las contraseñas por defecto del sistema*

* *No escribir la contraseña en ningún medio* y, si se hace, nunca identificarla como contraseña, o con otra información como el nombre de usuario o el sistema.

* *No enviar su contraseña por correo ni mencionarla en una conversación.*

Como en el caso anterior, si se hace, no hacerlo de forma implícita o proporcionando información de usuario o sistema.

* *No teclear la contraseña si alguien está observando.*

Como norma de cortesía no se suele mirar el teclado si alguien está tecleando su contraseña.

* *No mantener la contraseña indefinidamente.*

A pesar de realizar una buena elección es posible que se descubra: alguien puede haberlo visto al teclearlo, capturarlo mediante programas de escucha, instalar un troyano que detecte lo que se teclea, etc. En algunos casos un acceso fallido hace que se escriba el *login* del usuario en lugar de su nombre.

* *Es aconsejable renovar la contraseña*  una vez al mes y con una frecuencia no inferior a los tres meses

10.1.4 Ejemplos
"""""""""""""""

A título meramente informativo, se describen algunas técnicas básicas para la elección de contraseñas. Evidentemente, el uso de estas contraseñas no es nada aconsejable.

* 	Unir palabras cortas con números o símbolos:
	
	*	mi-palabra+clave	o	soy*yo

*	Usar un acrónimo de una frase sencilla de recordar:

		**EuldlMdcn**:	*En un lugar de la Mancha de cuyo nombre*

	Si es una frase desconocida aún mejor:

		**mPCeePqno**:	*Mi palabra clave es el password que no olvido*
*	Inventar una palabra sin sentido pero pronunciable:

		* gamounitos	

		* soceflos

* 	Añadir números o símbolos a una palabra cualquiera incrementa su seguridad:

		* 101gamounitos	
		* soce10flos

*	También se incrementa la seguridad reemplazando letras por números:

		* g4m0un1t0s	
		* m1-p4l4br4+cl4\/3



10.2 SSH en Microsoft Windows
##############################

El protocolo SSH permite establecer conexiones seguras entre dos ordenadores conectados mediante una red no segura como Internet. Una vez establecida la conexión entre los dos puntos, todo el tráfico entre ellos es cifrado, por lo que se dificulta la posibilidad de interceptarlo. Este protocolo reemplaza a varios de los protocolos existentes como telnet, rlogin, FTP, etc.

Magerit únicamente soporta como protocolo de conexión SSH. Sin embargo, es necesario configurar adecuadamente el cliente para permitir conexiones. La mayoría de las distribuciones Unix/Linux tienen soporte de SSH incluido por lo que no es necesario realizar ningún proceso de configuración. Sin embargo, en el caso de los sistemas Windows, es necesario instalar y configurar un par de herramientas.

10.3 PuTTy: cliente SSH
"""""""""""""""""""""""

El cliente recomendado para utilizar SSH en sistemas Windows es `PuTTY <http://www.putty.nl/>`_ , un programa libre cuyo código es la base de prácticamente la totalidad de las aplicaciones Windows que usan SSH.

Es posible descargar únicamente el ejecutable o un paquete de utilidades completo. En cualquiera de los dos casos, es necesario crear un perfil con la configuración apropiada. Para ello basta con replicar la configuración mostrada en la sigueiente imagen  y almacenarla. Aunque no es necesario, se aconseja configurar la codificación en UTF-8 (categoría *Window* -> *Translation* ).

.. figure:: ../../html/images/img81.jpg
   :align: center
   :alt: alternate text

   Configuración de la sesión

10.4 Intercambio de ficheros
"""""""""""""""""""""""""""""

La copia de información en Magerit también debe realizarse utilizando el protocolo SSH. Si se ha instalado el paquete completo de PuTTy se dispone de una aplicación de consola que permite realizar esta labor indicando el origen y el destino mediante la sintaxis


.. image:: ../../html/images/img82.jpg
   :align: center
  

También es posible utilizar la aplicación  `WinSCP <http://winscp.net/>`_ , que es una interfaz gráfica que permite las transferencias entre la máquina Windows y un servidor remoto. La configuración necesaria para Magerit puede verse en la siguiente imagen

.. figure:: ../../html/images/img83.jpg
   :align: center
   :alt: alternate text
   
   Configuración de WinSCP para intercambiar ficheros con Magerit



10.5 Conexiones gráficas
""""""""""""""""""""""""""

Las interfaces gráficas en los sistemas Linux utilizan una arquitectura cliente-servidor. La pantalla es gestionada por el denominado servidor X y las distintas aplicaciones son clientes de dicho servidor. Esta arquitectura permite ejecutar programas en cualquier máquina y presentar los resultados en el servidor de la máquina local.

Para hacer uso de esta funcionalidad desde Windows necesario instalar un servidor X. Existen varios servidores disponibles, siendo una opción muy recomendable `Xming <http://www.straightrunning.com/XmingNotes/>`_  o  `VcXsrv <http://sourceforge.net/projects/vcxsrv/>`_ , ambas aplicaciones libres.

Una vez instalado se debe ejecutar el servidor antes de utilizar aplicaciones gráficas. Es recomendable añadirlo a la lista de programas que se ejecutan al iniciar la sesión.

Tras tener el servidor activo en el sistema, es necesario configurar apropiadamente el cliente SSH para que permita las conexiones X11. Si se utiliza PuTTy como cliente de SSH, se debe habilitar la opción mostrada en la siguiente imagen

.. figure:: ../../html/images/img84.jpg
   :align: center
   :alt:  alternate text

   Activación del túnel para la conexión gráfica