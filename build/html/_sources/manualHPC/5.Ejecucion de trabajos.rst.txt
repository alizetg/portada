************************
5. Ejecución de trabajos
************************

La ejecución de trabajos en el sistema se gestiona, única y exclusivamente, mediante el gestor SLURM. Este planificador permite ejecutar en el supercomputador tanto trabajos secuenciales como paralelos, reservando los recursos (nodos) según las necesidades de los distintos trabajos. Este proceso dependerá de los recursos solicitados por el trabajo, la disponibilidad de dichos recursos en la máquina y las políticas de ejecución que se hayan definido.

Para poder enviar un trabajo al supercomputador es necesario definir sus características en un *Job Command File* o *jobfile*, especificando los recursos que deben reservarse. El gestor de colas se encargará de buscar y reservar los recursos indicados entre los disponibles, optimizando el uso de la máquina y reduciendo el tiempo de espera de los distintos usuarios. En resumen, el gestor se encarga de maximizar la eficiencia del supercomputador.

Por lo tanto, los pasos para poder ejecutar un trabajo en la máquina se resumen en:

1.	Conectarse a uno de los nodos interactivos.
2.	Preparar el ejecutable y los datos que se desee enviar al supercomputador.
3.	Preparar la definición del trabajo a enviar.
4.	Enviar el trabajo al gestor de colas.
5.	Esperar a que el sistema asigne recursos y ejecute la carga de trabajo.
6.	Recuperar los resultados y enviar nuevos trabajos.

5.1 Calidades de servicio
#########################

El acceso al sistema se controla mediante calidades de servicio *(Quality of Service, QoS)* que definen la prioridad y restricciones de acceso a los recursos. Cada usuario, en función del proyecto al que se encuentre adscrito, podrá tener acceso a un subconjunto de ellas que se notificará por correo en el momento de su activación. Cada usuario tendrá asignada una *Quality of Service (QoS)* para el envío de trabajos determinada por el proyecto al que pertenece.

   
.. image::  ../../html/images/img77.jpg
   :align: center


Todos los proyectos tienen asignada su correspondiente calidad de servicio en función de sus características.

5.2 Resumen de mandatos
#######################

Desde el punto de vista del usuario, existe una interfaz muy simple que se resume en las siguientes órdenes:

* **jobcancel:**

Cancela un trabajo que se encuentre en la cola de ejecución, esté esperando o ejecutando.

El trabajo puede estar tanto en espera (nunca llegará a ejecutar) como en ejecución. Para los trabajos en ejecución se abortarán los procesos y se contabilizarán los recursos utilizados hasta ese momento.

* **jobq:**

Muestra un resumen con el estado de los trabajos del usuario en el sistema desglosado según su estado.

* **jobstats:**

Muestra información sobre un trabajo en ejecución o ya ejecutado de manera simplificada y legible incluyendo las características del trabajo y el consumo aproximado de los recursos asignados. Además muestra un pequeño análisis orientativo sobre la eficacia en el uso de los recursos.

La información estadística únicamente se muestra para los pasos de los trabajos. Cada ejecución de srun se considera un paso por lo que si el jobfile no incluye ningún srun, no se dispondrá de estadísticas de uso de recursos.

* **jobsubmit:**

Envía un trabajo al planificador para su posterior ejecución.

5.3 Definición del trabajo
##########################

La definición de un trabajo se realiza mediante un fichero de texto que se denomina *Job Command File* o *jobfile*. Este fichero es al mismo tiempo un script que se encargará de iniciar la ejecución del trabajo. A este fichero de órdenes (script) se le añaden directivas especiales que indican el tipo de trabajo y los recursos que precisa. El gestor de recursos sólo procesará las directivas propias, que comienzan por #@, mientras que el resto de las líneas se considerarán comentarios.

::

   #!/bin/bash
	 #----------------------- Start job description -----------------------
	 #@ arch             = (ppc64|power|intel)
	 #@ initialdir       = /home/[project_id]/[data_dir]
	 #@ output           = out-%j.log
	 #@ error            = err-%j.log
	 #@ total_tasks      = [number of tasks]
	 #@ wall_clock_limit = [hh:mm:ss]
	 #------------------------ End job description ------------------------

	 #-------------------------- Start execution --------------------------

	 # Run our program
	 srun ./[myprogram]

	 #--------------------------- End execution ---------------------------


La plantilla de definición de un trabajo se compone de:

•	La primera línea indica el shell que debe utilizarse para interpretar la zona ejecutable del mismo

•	Seguidamente se incluyen todas las directivas que definen el trabajo. Estas directivas se consideran comentarios por el shell (precedidos de #).

•	Finalmente se dispone el código ejecutable que prepara, lanza la ejecución y recupera los resultados.

SLURM define un conjunto de variables que pueden utilizarse en el propio *jobfile*:


**SLURM_JOBID:**	Identificador del trabajo que se ejecuta.

**SLURM_LOCALID:**	Indica el identificador local de la tarea en el nodo.

**SLURM_NNODES:**	Número de nodos asignados al trabajo.

**SLURM_NODEID:**	Identificador relativa del nodo para el trabajo actual.

**SLURM_NODELIST:**	Listado de nodos en los que está ejecutando el trabajo.

**SLURM_NPROCS:**	Número total de procesos en el trabajo.

**SLURM_PROCID:**	Identificador del proceso en el trabajo (MPI rank) para el proceso actual.

5.3.1 Directivas
""""""""""""""""

Las directivas permiten indicar al planificador las características del trabajo que se desea ejecutar. Todas las directivas tienen el formato **#@ directiva [= valor]** y pueden utilizar las variables de entorno del propio sistema.

5.3.2 Directivas obligatorias
"""""""""""""""""""""""""""""

Todos los *jobfile* deben especificar obligatoriamente las directivas:

* total_tasks=N **[Directiva obligatoria]**

Indica el número de procesos que se necesitan. A cada proceso se le asignarán tantas CPUS como indique la directiva *cpus_per_task* (si se omite será 1 CPU). El gestor de colas se encargará de buscar y reservar recursos libres hasta completar este número.

El valor máximo de esta directiva está limitado por la calidad de servicio que tenga asignada el proyecto en ese momento.

* wall_clock_limit=hh:mm:ss **[Directiva obligatoria]**

Especifica el tiempo máximo que el proceso va a estar ejecutando.

El valor máximo de esta directiva está limitado por la calidad de servicio que tenga asignada el proyecto en ese momento.

El gestor tiene en cuenta este valor a la hora de planificar trabajos. Si el valor especificado para *wall_clock_limit* es mucho mayor que el tiempo real que consumirá la tarea, se verá penalizada en el tiempo de espera antes de empezar a ejecutar y no podrá aprovechar el sistema de *backfilling*.

Se recomienda establecer un mínimo de 15 minutos para que no se 
produzcan interferencias con el sistema de ahorro energético.

.. topic:: Nota:
	
	Si el proceso supera el tiempo indicado, el gestor de colas abortará su ejecución. 

	Es recomendable establecer una pequeña holgura para tener cierto margen de error, aunque es una buena política y beneficia a todos los usuarios, el acotar lo máximo posible este valor.

5.3.3 Directivas generales
""""""""""""""""""""""""""

Asimismo, es posible configurar en detalle las ejecuciones mediante el uso de las directivas:

* *arch=tipo* **[Directiva recomendada para usuarios de la UPM]**

  Indica la arquitectura que se precisa para el trabajo. Los únicos valores permitidos son:
   
    * *ppc64*

      		Equipos con procesadores Power7.
      		*Esta opción sólo está disponible para usuarios RES. Además es la única válida para estos usuarios.*

    * *power*

      		Equipos con procesadores Power7. 
      		*Opción por defecto en caso de no incluir esta directiva, para usuarios de la UPM.*

    * *intel*  		

            Equipos con procesadores Intel(x86_64).

* *error=fichero*

  Indica dónde se redirige la salida de error del trabajo. Este fichero recogerá la salida combinada de todos los procesos que ejecutan en el trabajo, directa o indirectamente, en cualquiera de los nodos asignados al trabajo.

  Si en el nombre se añade un %j se reemplazará por el identificador del trabajo asignado por el planificador.

  La actualización de esta redirección es prácticamente inmediata.

* *initialdir=ruta_directorio* **[Directiva recomendada]**

  Permite establecer el directorio de trabajo del script y todas las rutas especificadas (output, error...) se consideran relativas a este directorio inicial.

  Si no se especifica se considerará el directorio de trabajo (.) en el momento de enviar el trabajo.
  
  Es aconsejable definir un valor absoluto para esta directiva ya que, el uso de rutas relativas al directorio de trabajo actual, puede generar confusión, errores en la ejecución o pérdida de datos.

* *requeue=yes|no*

  Activa o desactiva el re-encolado automático en el caso de un fallo hardware de un nodo de cómputo asignado. Si no se indica, los trabajos no serán re-encolados (requeue=no).

  	* *yes*
  	   El trabajo será re-encolado y se planificará nuevamente como si no hubiera ejecutado

  	* *no*
  	   El trabajo será abortado y no se volvera a planificar.


  El trabajo re-encolado tendrá exactamente la misma configuración e identificador por lo que reemplazará las redirecciones de entrada/salida, ficheros…

.. topic:: Nota:

     A efectos de contabilidad, nunca se borrará el consumo realizado por el trabajo que sufrió el fallo. Por lo tanto, el consumo final será la suma del realizado en el primer intento y de los sucesivos re-encolados (si se activa utilizando esta directiva).


* *output=fichero*
  
  Indica dónde se redirige la salida estándar del trabajo. Este fichero recogerá la salida combinada de todos los procesos que ejecutan en el trabajo, directa o indirectamente, en cualquiera de los nodos asignados al trabajo.
  
  Si en el nombre se añade un %j se reemplazará por el identificador del trabajo asignado por el planificador.

  La actualización de esta redirección no es inmediata pudiendo sufrir retardos.

* *tasks_per_node=N*

  Indica cuántas tareas deben asignarse como máximo en cada nodo. El gestor de colas asignará bloques de *tasks_per_node* tareas a cada uno de los nodos asignados.
	
  Utilizando esta directiva en combinación con la directiva cpus_per_task es posible reservar nodos en exclusiva dejando libres procesadores. Para ello el producto *tasks_per_node×cpus_per_task* debe dar como resultado 16.

5.3.4 Uso de OpenMP

El sistema permite el envío de trabajos híbridos MPI-OpenMP: se ejecutarán tantos procesos MPI como indique la directiva total_tasks y cada una de ellas desplegará varios hilos OpenMP.

A efectos de contabilidad, se consideran los recursos asignados a la ejecución. Cada hilo se contabiliza como una CPU ocupada, por lo que las horas consumidas por cada hilo son acumuladas.

Es decir, se contabiliza el equivalente a total_tasks×cpus_per_task tareas durante el tiempo que dure la ejecución.

Para activar esta funcionalidad, es necesario añadir la directiva:

	* *cpus_per_task=N*

       Indica el número de threads OpenMP que deben lanzarse por cada tarea (todos los hilos de cada tarea ejecutan en el mismo nodo). Debido a la arquitectura de cada nodo, este valor deberá ser una potencia de 2 entre 2 y 16

.. topic:: Nota:

	El sistema se encarga automáticamente de definir la variable OMP_NUM_THREADS adecuadamente. Modificarla manualmente en el jobfile puede tener consecuencias imprevisibles o indeseables.

5.4 Trabajos múltiples
######################

Asimismo, es bastante usual tener que enviar conjuntos de trabajos relacionados o, incluso, con dependencias más o menos complejas entre ellos. Según las necesidades de cómputo existen múltiples prácticas de envío de trabajos al sistema. Sin embargo, existen tres modelos básicos que son comúnmente empleados: trabajos múltiples, encadenados (chains) y por pasos (steps).

Adicionalmente, Magerit dispone de un cuarto mecanismo específico para la ejecución masiva de trabajos secuenciales.


5.4.1. Trabajos de ejecución múltiple
"""""""""""""""""""""""""""""""""""""
Consiste en utilizar un único trabajo que ejecute varios programas diferentes utilizando para ello el mismo conjunto de nodos. Con este fin basta con incorporar al listado básico varias llamadas srun similares que ejecuten los distintos programas.

:: 

   #!/bin/bash
   #----------------------- Start job description -----------------------
   #@ arch             = (ppc64|power|intel)
   #@ initialdir       = /home/<project_id>/<data_dir>
   #@ output           = out-%j.log
   #@ error            = err-%j.log
   #@ total_tasks      = <number of tasks>
   #@ wall_clock_limit = <hh:mm:ss>
   ##------------------------ End job description ------------------------
   ##-------------------------- Start execution --------------------------
   ## First program / First run
   srun ./<myprogram1> <arg1>
   ## First program / Second run
   srun ./<myprogram1> <arg2>
   ## Second program / First run
   srun ./<myprogram2> <arg1>
   ## Second program / Second run
   srun ./<myprogram2> <arg2>
   # ... More executions ...
   ##--------------------------- End execution ---------------------------

Esta técnica tiene como principal ventaja la supresión del tiempo de espera en cola para la segunda y subsiguientes ejecuciones. Sin embargo, tiene como inconveniente que es necesario incrementar la solicitud de recursos (solicitar el número máximo de procesadores de todos los trabajos, sumar el tiempo de ejecución de todos ellos...) lo que incrementa el tiempo de espera para la primera ejecución.

Normalmente esta técnica está indicada para ejecuciones del mismo programa con distinto número de argumentos y de corta duración (menos de un día). Al empaquetarlos juntos, se reduce el tiempo medio de espera y no se reciben tantas penalizaciones por el envío convulsivo de trabajos.


5.4.2 Trabajos encadenados (chains)
"""""""""""""""""""""""""""""""""""""""

Los trabajos encadenados permiten ejecutar un conjunto de trabajos cuando existen dependencias entre ellos. Su funcionamiento es similar al de trabajos individuales pero al finalizar la ejecución el propio trabajo envía, y en ocasiones genera, el trabajo que debe continuar la labor.

:: 

   #!/bin/bash
   #----------------------- Start job description -----------------------
   #@ arch             = (ppc64|power|intel)
   #@ initialdir       = /home/<project_id>/<data_dir>
   #@ output           = out-%j.log
   #@ error            = err-%j.log
   #@ total_tasks      = <number of tasks>
   #@ wall_clock_limit = <hh:mm:ss>
   ##------------------------ End job description ------------------------
   ##-------------------------- Start execution --------------------------
   ## Run our program
   srun ./<myprogram1>
   ## Queue next step
   jobsubmit <jobfile>
   ##--------------------------- End execution ---------------------------


Esta técnica puede ser empleada para superar las restricciones de tiempo máximo en cola: la aplicación en ejecución almacena su estado horas antes de cumplirse el plazo máximo y se reenvía, recuperando el estado en el siguiente trabajo y continuando la ejecución.

Aunque los trabajos encadenados abordan dependencias simples entre procesos, no permiten una solución sencilla a las dependencias múltiples.


5.4.3 Trabajos secuenciales masivos

En algunos casos es necesario ejecutar baterías de trabajos secuenciales en los que el mismo código debe ejecutarse con múltiples combinaciones de parámetros (en ocasiones miles de combinaciones) y cada una de las ejecuciones es completamente independiente de las demás. Aunque este tipo de ejecución puede realizarse utilizando alguno de los esquemas de trabajos múltiples, el sistema no está optimizado para ejecuciones de esta naturaleza.

Para mejorar el rendimiento de este tipo de trabajos se ha desarrollado un gestor de trabajos secuenciales (*seqfarmer*), cuyo uso recomendamos encarecidamente. Este mecanismo utiliza un algoritmo de cola única mediante un *pool* de nodos de cómputo (similar a lo que es habitual ver en la gestión de colas de supermercados).

El “granjero” se encarga de repartir cada uno de los trabajos secuenciales a los nodos disponibles hasta ejecutar todos los trabajos indicados. Debido a la falta de dependencias entre ellos, este programa proporciona un incremento de rendimiento casi proporcional al número de procesadores utilizados.

Para utilizar esta aplicación se precisa definir dos ficheros.


* **Jobfile**

	En el que se define la ejecución de la aplicación *seqfarmer*. Se trata de un *jobfile* estándar que lanza la ejecución de la aplicación indicando el fichero en el que se especifican qué trabajos secuenciales deben ejecutarse.

::

   #!/bin/bash
   ##----------------------- Start job description -----------------------
   #@ arch             = (ppc64|power|intel)
   #@ initialdir       = /home/<project_id>/<data_dir>
   #@ output           = out-%j.log
   #@ error            = err-%j.log
   #@ total_tasks      = <number of tasks>
   #@ wall_clock_limit = <hh:mm:ss>
   ##------------------------ End job description ------------------------
   ##-------------------------- Start execution --------------------------
   ## Run our program
   srun seqfarmer -f=<tasksfile>
   ##--------------------------- End execution ---------------------------


* **Listado de trabajos.**

Lista los trabajos secuenciales a ejecutar. Este fichero es el que se le pasara como parámetro a la aplicación *seqfarmer* en el *jobfile* definido anteriormente.

::

   ## Each line is a single sequential job
   #@ initialdir = path/to/base/dir
   #@ output     = path/to/outfile.log
   #@ error      = path/to/errorfile.log
   ./myprogram 0 0 0
   #@ output     = path/to/outfile.log
   ./myprogram 0 0 1
   ./myprogram 0 1 0
   ./myprogram 0 1 1
   #@ output     = path/to/outfile.log
   #@ error      = path/to/errorfile.log
   ./myprogram 1 0 0
   ./myprogram 1 0 1
   ./myprogram 1 1 0
   #@ output     = path/to/outfile.log
   #@ error      = path/to/errorfile.log
   ./myprogram 1 1 1
   ./myprogram 1 0 1 > path/to/outfile.log 2> path/to/errorfile.log

Este fichero implementa extensiones de algunas de las directivas empleadas en el *jobfile* (initialdir, output y error). Nótese que las directivas del *jobfile* afectan a todos los procesos secuenciales, mientras que las del *tasksfile* sólo afectan al proceso secuencial inmediatamente posterior a su definición y sustituyen a las definidas en el *jobfile*.

Las rutas relativas definidas en el *tasksfile*, lo son respecto a las rutas definidas en el jobfile. Las rutas absolutas se respetan en su integridad.

Las directivas implicadas son:

1. **#@initialdir:**
    
* *jobfile:*

  Esta directiva es opcional. Si no se inicializa su valor por omisión es el directorio desde el que se ha enviado el *jobfile*.

* *taskfile:*

  Esta directiva es opcional. Por omisión, si no se inicializa su valor, se toma la ruta actual definida en el *jobfile*. 

  Como resultado de la ruta de cada ejecución secuencial es:

:: 

   ruta = jobfile_path/#@ initialdir (del jobfile)/#@ initialdir (del tasksfile)

2. **#@ output y #@ error**

* *jobfile:*

  Si no se inicializan estas directivas sus valores por omisión se corresponden con las salidas por defecto:

::

   salida output = jobfile_path/#@ output (del jobfile)
   salida error = jobfile_path/#@ error (del jobfile



* *taskfile:*

  La definición de estas directivas **sustituye** las definidas en el *jobfile*:

::

	salida output = jobfile_path/#@ output (del tasksfile)
	salida error = jobfile_path/#@ error (del tasksfile)


También es posible usar redirecciones estilo bash, tal y como se ilustra en la última línea del ejemplo del tasksfile. Este sistema de redirección es compatible con el resto de las directivas, siendo prioritarias las de bash.

El número de procesadores a solicitar en el jobfile (total_taks) es independiente del número de trabajos secuenciales que se desean ejecutar, pues lo que se define es el pool de procesadores a utilizar. Sin embargo, el número máximo de procesos secuenciales simultáneamente en ejecución estará limitado a tantos como procesadores se hayan solicitado menos uno (total_tasks - 1), pues se reserva un procesador para labores de coordinación y gestión del paralelismo. Como mínimo es necesario utilizar 3 tareas (1 gestor y 2 trabajadores).

Es aconsejable que el número de procesos secuenciales que se vayan a ejecutar (descritos en tasksfile) sea múltiplo del número de procesadores menos uno (total_tasks - 1) para aprovechar al máximo el paralelismo. También es aconsejable listar las tareas con mayor duración las primeras ya que aprovecharán mejor los recursos.

También es importante reseñar que se debe incrementar el wall_clock_limit para permitir la ejecución usando la fórmula:



.. image:: ../../html/images/img78.JPG
   :align: center


Por ejemplo, si se lanzan 20 trabajos a 6 procesadores (5 ejecuciones y un coordinador), se ejecutarán unos 4 ciclos de ejecución 

.. image:: ../../html/images/img79.jpg
   :align: center

Es decir, el **wall_clock_limit** deberá ser 4 veces el de una ejecución individual más un pequeño margen de holgura.
