.. UPMDrive documentation master file, created by
   sphinx-quickstart on Thu Feb  1 12:12:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación del CeSViMa
=========================================



A finales del año 2004, la Universidad Politécnica de Madrid (UPM) decide crear un centro para suplir la carencia de recursos existentes para investigación. Fruto de esta iniciativa, nace el CeSViMa (Centro de Supercomputación y Visualización de Madrid).
"Cambiar OJO"



.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   /portada/index
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
